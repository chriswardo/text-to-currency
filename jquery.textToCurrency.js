/*

TODO:

- Test across browsers
- A mode to hide the original so that it seems to swap currencies.
- option to add to currencies $.extend
- maybe change the classes for the output or something?
- Chrome extension? WordPress plugin?

*/

(function( $ ) {


  console.log( "textToCurrency v0.8 by Chris Ward: https://gitlab.com/chriswardo/text-to-currency");

  // Currency definitions
  var CURRENCIES = {
    "GBP": {
      before: [
        "\\u00A3",
        "GBP\\s*"
      ],
      after: [
        "\\s*GBP",
        ["p",0.01],
        ["\\s*pence",0.01]
      ],
      symbol: "&pound;",
      symbol_position: "before"
    },
    "MYR": {
      before: [
        "MYR\\s*",
        "RM\\s*"
      ],
      after: [
        "\\s*RM",
        "\\s*MYR",
        "\\s*ringgit"
      ],
      symbol: "RM",
      symbol_position: "before"
    },
    "IDR": {
      before: [
        "IDR\\s*",
        "Rp\\s*"
      ],
      after: [
        "\\s*Rp",
        "\\s*IDR",
        "\\s*rupiah"
      ],
      symbol: "Rp ",
      symbol_position: "before"
    },
    "TWD": {
      before: [
        "TWD\\s*",
        "\\u0024\\s*",
        "NT\\u0024\\s*"
      ],
      after: [
        "\\s*TWD"
      ],
      symbol: "NT$",
      symbol_position: "before"
    },
    "VND": {
      before: [
        "VND\\s*"
      ],
      after: [
        "d",
        "\\s*dong",
        "\\u20AB",
        "\\s*VND",
        ["k",1000]
      ],
      symbol: "&#8363;",
      symbol_position: "after"
    },
    "JPY": {
      before: [
        "JPY\\s*",
        "\\uA5",
        "Y"
      ],
      after: [
        "\\s*yen",
        "\\uA5",
        "\\s*JPY",
        ["k",1000]
      ],
      symbol: "&#165;",
      symbol_position: "before"
    },
    "KRW": {
      before: [
        "KRW\\s*",
        "\\u20A9",
        "W"
      ],
      after: [
        "\\s*won",
        "\\u20A9",
        "\\s*KRW",
        ["k",1000]
      ],
      symbol: "&#8361;",
      symbol_position: "before"
    },
    "THB": {
      before: [
        "THB\\s*"
      ],
      after: [
        "B",
        "\\s*baht",
        "\\a0E3F",
        "\\s*THB"
      ],
      symbol: "&#3647;",
      symbol_position: "after"
    },
    "USD": {
      before: [
        "USD\\s*",
        "\\u0024\\s*"
      ],
      after: [
        "\\s*dollar",
        "\\s*dollars",
        "\\s*\\u0024",
        "\\s*USD",
        ["c",0.01],
        ["\\u00a2",0.01],
        ["\\s*cents",0.01]
      ],
      symbol: "$",
      symbol_position: "before"
    },
    "EUR": {
      before: [
        "EUR\\s*",
        "\\u20ac\\s*"
      ],
      after: [
        "\\s*euros",
        "\\s*euro",
        "\\s*\\u20ac",
        "\\s*EUR"
      ],
      symbol: "&euro;",
      symbol_position: "before"
    },
    "AUD": {
      before: [
        "AUD\\s*",
        "\\u0024\\s*"
      ],
      after: [
        "\\s*dollar",
        "\\s*dollars",
        "\\s*\\u0024",
        "\\s*AUD",
        ["c",0.01],
        ["\\u00a2",0.01],
        ["\\s*cents",0.01]
      ],
      symbol: "$",
      symbol_position: "before"
    },
    "NZD": {
      before: [
        "NZD\\s*",
        "\\u0024\\s*"
      ],
      after: [
        "\\s*dollar",
        "\\s*dollars",
        "\\s*\\u0024",
        "\\s*NZD",
        ["c",0.01],
        ["\\u00a2",0.01],
        ["\\s*cents",0.01]
      ],
      symbol: "$",
      symbol_position: "before"
    },
    "CAD": {
      before: [
        "CAD\\s*",
        "\\u0024\\s*"
      ],
      after: [
        "\\s*dollar",
        "\\s*dollars",
        "\\s*\\u0024",
        "\\s*CAD",
        ["c",0.01],
        ["\\u00a2",0.01],
        ["\\s*cents",0.01]
      ],
      symbol: "$",
      symbol_position: "before"
    },
    "SGD": {
      before: [
        "SGD\\s*",
        "\\u0024\\s*",
        "S\\u0024\\s*"
      ],
      after: [
        "\\s*dollar",
        "\\s*dollars",
        "\\s*\\u0024",
        "\\s*SGD",
        ["c",0.01],
        ["\\u00a2",0.01],
        ["\\s*cents",0.01]
      ],
      symbol: "S$",
      symbol_position: "before"
    },
    "HKD": {
      before: [
        "HKD\\s*",
        "\\u0024\\s*",
        "HK\\u0024\\s*"
      ],
      after: [
        "\\s*dollar",
        "\\s*dollars",
        "\\s*\\u0024",
        "\\s*HKD",
        ["c",0.01],
        ["\\u00a2",0.01],
        ["\\s*cents",0.01]
      ],
      symbol: "HK$",
      symbol_position: "before"
    }
  };

  // When finding all text nodes, stop when you get to one of these html elements
  var EXCLUDE_TAGS = [ "textarea", "option", "input", "iframe" ];




  var quotes = {};

  function createCurrencyRegex( currency ) {
    currency.regex = [];

    $.each(currency.before, function( i, r ) {
      if ( r.constructor === Array ) {
        currency.regex.push( [ r[0] + "\\c", r[1] ] );
      }
      else {
        currency.regex.push( [ r + "\\c", 1 ] );
      }

    });
    $.each(currency.after, function( i, r ) {
      if ( r.constructor === Array ) {
        currency.regex.push( [ "\\c" + r[0], r[1] ] );
      }
      else {
        currency.regex.push( [ "\\c" + r, 1 ] );
      }

    });
  }

  function createCurrencyRegexRange( currency ) {
    currency.regexRange = [];

    $.each(currency.before, function( i, r ) {
      if ( r.constructor === Array ) {
        currency.regexRange.push( [ r[0] + "\\c" + "\\s*\-\\s*(" + r[0] + ")?" + "\\c", r[1] ] );
      }
      else {
        currency.regexRange.push( [ r + "\\c" + "\\s*\-\\s*(" + r + ")?" + "\\c", 1 ] );
      }
    });
    $.each(currency.after, function( i, r ) {
      if ( r.constructor === Array ) {
        currency.regexRange.push( [ "\\c(" + r[0] + ")?\\s*\-\\s*\\c" + r[0], r[1] ] );
      }
      else {
        currency.regexRange.push( [ "\\c(" + r + ")?\\s*\-\\s*\\c" + r, 1 ] );
      }
    });
  }

  // Construct regex
  $.each(CURRENCIES, function( code, currency ) {
    createCurrencyRegex( currency );
  });

  // Construct range regex
  $.each(CURRENCIES, function( code, currency ) {
    createCurrencyRegexRange( currency );
  });

  function formatNumber(val,fixed){
    val = parseFloat(val).toFixed( fixed !== undefined ? fixed : 2 );
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, "$1"+","+"$2");
    }
    return val;
  }

  function htmlEncode(rawStr) {
    return rawStr.replace(/./gim, function(i) {
      return "&#"+i.charCodeAt(0)+";";
    });
  }

  function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
  }

  // Go through and find all the relevant text nodes
  function getAllTextNodes( parent ) {
    if ( EXCLUDE_TAGS.indexOf( ($(parent)[0].tagName||"").toLowerCase() ) >= 0 || $(parent).hasClass("currency-block") ) {
      return [];
    }

    var nodes = [];
    $(parent).contents().each(function() {
      if ( this.nodeType == 3 ) {
        nodes.push( this );
      }
      else {
        nodes = nodes.concat(getAllTextNodes( this ));
      }
    });
    return nodes;
  }

  $.fn.textToCurrency = function( options ) {

    var settings = $.extend(true, {
      from: ["GBP"], // string or array
      to: ["USD"], // string or array
      output: {
        type: "symbol", // either "code" (23 THB) or "symbol" (23฿)
        prepend: " &rarr; ",
        append: "",
        join: " - ",
        css: {
          fontSize: "90%"
        }
      },
      replace: false // either "code" (23 THB), "symbol" (23฿) or false
    }, options );


    var parent = this;

    if ( parent.length == 0 ) return this; // nothing to do

    if ( typeof settings.from == "string" ) settings.from = [ settings.from ];
    if ( typeof settings.to == "string" ) settings.to = [ settings.to ];

    for ( var c = 0; c < settings.from.length; c++ ) {
      if ( settings.from[c] == settings.to[0] || settings.from[c].length == 0 ) continue; //No point

      //Reset previous ones
      parent.find(".currency-original[data-currency=" + settings.from[c] + "]").parent().each( function() {
        var orig = $(this).find(".currency-original").attr("data-original-str");
        $(this).replaceWith( orig );
      });

      var textNodes = getAllTextNodes(parent);

      $(textNodes).each(function() {
        var e = $( this );
        if ( e.text().trim().length == 0 ) return;

        var content = e.text();
        var original_content = content;
        var thiscontent = content;



        if ( !CURRENCIES[settings.from[c]] ) { // not set
          CURRENCIES[settings.from[c]] = { before: [settings.from[c]+"\\s*"], after: ["\\s*"+settings.from[c]] }; //at least have "1 VND"
          createCurrencyRegex( CURRENCIES[settings.from[c]] );
          createCurrencyRegexRange( CURRENCIES[settings.from[c]] );
        }
        var currency_regex = CURRENCIES[settings.from[c]].regexRange.concat(CURRENCIES[settings.from[c]].regex);

        for ( var i = 0; i < currency_regex.length; i++ ) {
          var re = new RegExp(currency_regex[i][0].replace(/\\c/g,"([0-9]+(?:(?:[0-9]{0,2}(?:\\,[0-9]{3})*)|[0-9]+)?(?:\\.[0-9]{1,2})?)"),"gi");

          var m;
          var counter = 0; //make sure it doesn't loop forever
          var thiscontentloop = thiscontent;
          var matches = [];
          while ((m = re.exec(thiscontentloop)) !== null && counter < 100) {

            counter++;
            if (m) {
              matches.push(m);
            }
          }


          matches = matches.sort( function(a, b) {
            return b[0].length - a[0].length;
          });

          $.each( matches, function( index, m ) {
            var numregex = new RegExp(/^[0-9][0-9\,\.]*$/);
            var numbers = $.makeArray(m).filter(function(val,index) { return (val != undefined && val.match(numregex) ); }).map(function(val,index) { return val.replace(/,/gi,"") });

            var prevCharIndex = m.index-1;
            var prevChar = m.input.substr( prevCharIndex, 1);

            var nextCharIndex = m.index+m[0].length;
            var nextChar = m.input.substr( nextCharIndex, 1);

            if ( !( nextCharIndex < m.input.length && nextChar.match(/[a-z]/i) ) && !( prevCharIndex >= 0 && prevChar.match(/[a-z]/i) ) ) {
              var from = m[0].trim();
              var fromregex = new RegExp(escapeRegExp(m[0].trim()),"g");
              var from_formatted = htmlEncode(from);
              if ( settings.replace ) {
                var formatted_numbers = numbers.map( function(n) {
                  return formatCurrency( n*currency_regex[i][1], settings.from[c], settings.replace );
                });
                from_formatted = formatted_numbers.join( settings.output.join );
              }
              var to = '<span class="currency-block" data-currency-fromto="' + settings.from[c] + settings.to[0] + '"><span class="currency-original" data-amount="' + numbers.join(',') + '" data-currency="' + settings.from[c] + '" data-unit="' + currency_regex[i][1] + '" data-original-str="' + htmlEncode(from) + '">' + from_formatted + '</span><span class="currency-converted" data-currency="' + settings.to[0] + '"></span></span>';

              content = content.replace(fromregex,to);
              thiscontent = thiscontent.replace(fromregex,""); //So it can't be matched again
            }

          });
        }


        if ( content != original_content ) e.replaceWith( $("<span>" + content + "</span>") );

      });
    }


    if ( !quotes.USDGBP ) {
      var url = false;
      if ( settings.quotes ) {
        quotes = settings.quotes;
        updateQuotes(parent, settings);
        return this;
      }
      else if ( settings.key ) {
        url = "http://www.apilayer.net/api/live?access_key=" + settings.key + "&format=1";
      }
      else if ( settings.url ) {
        url = settings.url;
      }
      else {
        console.log("Error: You must specify the currency data (quotes), your Currencylayer API key (key) or an alternative address for the data (url).");
        return false;
      }

      var callback = $.proxy( function( context, data ) {

        if ( data.success ) {
          quotes = data.quotes;
          updateQuotes(parent, context.settings);
        }
        else if ( data.error && data.error.info ) {
          console.log("Error with the currencylayer API. " + data.error.info );
        }
        else {
          console.log("Error with the currencylayer API." );
        }
      }, this, { settings: settings });

      $.getJSON( url, callback ).fail(function(err) {
        console.log("Error accessing the currencylayer API");
      });
    }
    else {
      updateQuotes(parent, settings);
    }

    return this;

  };

  function formatCurrency( amount, currency, type ) {
    // TODO: deal with units
    amount = formatNumber(amount,2);
    if ( amount.substr(-3) == ".00" ) amount = amount.substr(0,amount.length-3);
    if ( type == "symbol" && CURRENCIES[currency] ) return ( CURRENCIES[currency].symbol_position == "before" ? CURRENCIES[currency].symbol : "" ) + amount + ( CURRENCIES[currency].symbol_position == "after" ? CURRENCIES[currency].symbol : "" );
    else return amount + " " + currency;

  }

  function updateQuotes(parent, settings) {

    for ( var c = 0; c < settings.from.length; c++ ) {
      parent.find(".currency-block[data-currency-fromto=" + settings.from[c] + settings.to[0] + "]").each(function() {
        var amounts = $(this).find(".currency-original").attr("data-amount").split(",");
        var from = $(this).find(".currency-original").attr("data-currency");
        var to = $(this).find(".currency-converted").attr("data-currency");
        var unit = parseFloat($(this).find(".currency-original").attr("data-unit"));

        if ( !quotes["USD"+from] ) {
          console.log( "Error: Currency " + from + " not found." );
        }
        else if ( !quotes["USD"+to] ) {
          console.log( "Error: Currency " + to + " not found." );
        }
        else {

          var rate = 1/(parseFloat(quotes["USD"+from])/parseFloat(quotes["USD"+to]));
          var converts = $.map( amounts, function( val, index ) {
            var output = formatNumber( parseFloat(val.replace(",",""))*rate*unit, (rate>1000?0:2) ); // if rate is over 1000, then don't bother with decimal places

            //if ( settings.output.type == "symbol" && CURRENCIES[to] ) output = ( CURRENCIES[to].symbol_position == "before" ? CURRENCIES[to].symbol : "" ) + output + ( CURRENCIES[to].symbol_position == "after" ? CURRENCIES[to].symbol : "" );
            //else output = output + " " + to;

            //output =

            return formatCurrency( output, to, settings.output.type );
          } );

          var ratefrom = 1;
          if ( rate < 0.01) ratefrom = 100;
          if ( rate < 0.001) ratefrom = 1000;
          if ( rate < 0.0001) ratefrom = 10000;

          $(this).find(".currency-converted").attr("title",formatNumber(ratefrom,0)+" "+from+" = "+(rate*ratefrom).toFixed(2)+" "+to).css(settings.output.css).html( "<!--googleoff: snippet-->" + settings.output.prepend + converts.join( settings.output.join ) + settings.output.append + "<!--googleon: snippet-->" );
        }
      });
    }

  }

}( jQuery ));
