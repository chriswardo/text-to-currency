# Text to Currency jQuery plugin

*Display inline currency conversions in text anywhere in an html document or element.*

This script detects references to currencies in the text of a webpage and displays the converted amount. It can also handle ranges (such as $1.50-2.50).

It could be used for travel blogs or any website where specific currency is mentioned in the content, allowing users from around the world the have their own currency displayed alongside the original.


## Demos

* [Demo site](https://chrisward.co.uk/code/text-to-currency/)
* Used on [The Travellist](https://thetravellist.net)

## Example code

```js
$('.content').textToCurrency({
  from: "MYR",
  to: "GBP",
  url: "/currencies.json",
  output: {
    type: "symbol",
    prepend: " &rarr; ",
    append: "",
    css: {
      color: 'red',
      opacity: 0.5,
      fontSize: '80%'
    }
  },
  replace: false
});
```

## Example of text to match

- **"130 USD"**: Currency codes will match.
- **"$9.95"**: So will currency symbols.
- **"£10-30"**: Ranges are identified and both values are converted.


## Options

- **from**: The currency code of the source currency (e.g. "MYR" for Malaysian Ringgit). Can be an array of codes to convert multiple at once.
- **to**: The currency code of the currency you want to convert to (e.g. "GBP" for British Pounds).
- **url**: The URL of the currencies JSON file (see below).
- **quotes**: The currencies JSON as an object (see below).
- **key**: Your Currencylayer API key (see below).
- **output**:
  - **type**: Either 'symbol' (to use the currency symbol in the output, like "£5.64") or 'code' (to use the currency code in the output, like "5.64 GBP").
  - **prepend**: Text before the output currency (such as an opening bracket).
  - **append**: Text after the output currency (such as a closing bracket).
  - **join**: A string to join currency ranges (such as " - ")
  - **css**: Object of CSS settings to apply to the output currency.
- **replace**: How to replace the original currency text. Either 'symbol' ("£5.64"), 'code' ("5.64 GBP") or false (to leave it untouched).


## Currency JSON

The script requires the currency data be given in one of four ways:

1. By providing an API key for currencylayer.com - which will fetch the data live for each page load (using the **key** option).
2. By providing the URL (relative or absolute) of the JSON file, in the same format as the API output from currencylayer.com (using the **url** option).
3. By passing the data object directly (using the **quotes** option).
4. By setting the quotes variable in the textToCurrency js file. This would be the fastest option but means updating the script regularly.


## Currencies configured so far...

- GBP (UK)
- USD (USA)
- MYR (Malaysia)
- VND (Vietnam)
- THB (Thailand)
- NZD (New Zealand)
- AUD (Australia)
- CAD (Canada)
- SGD (Singapore)
- JPY (Japan)
- IDR (Indonesia)
- TWD (Taiwan)
- HKD (Hong Kong)
- KRW (South Korea)
- EUR (Europe)
